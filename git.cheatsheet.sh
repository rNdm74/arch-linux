## Basic git Cheatsheet ##

# Get the status of the current git changes
git status

# Download the latest from your repo
git pull

# Stage new/changed files/folders to be uploaded to your repo
git add *

# Stage files/folders to be deleted from you repo Note: Can be tricky
git rm * | -r FolderName

# Tell git what changes you have made
git commit -m "Message"

# Upload all changes to your repo
git push
