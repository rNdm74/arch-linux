## Pacman Cheatsheet ##

#Update the system
pacman -Syu

# Install a program 
pacman -S

# Search for a program
pacman -Ss

# Remove a program
pacman -Rsn

# List installed programs
pacman -Q

# Query unused programs
pacman -Qdt

# List default programs
pacman -Qe

# List arch user repo installed programs
pacman -Qm
