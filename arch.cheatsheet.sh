# Install reference guide
https://wiki.archlinux.org/index.php/Installation_guide

# Connect to the Internet
ping archlinux.org

# Update the system clock
timedatectl set-ntp true

# Partition the disks
fdisk -l

# Choose depends on boot mode type
function UEFI_Partitioning
{
    # Check UEFI
    efivar -l
    
    # Check Parition Table
    lsblk
    
    # Check Disk Paritions
    fdisk -l
    
    if !Partitions_Exist
        fdisk /dev/sdX # Main Disk for System Install
        
        # Create EFI Boot Parition
        # Press g
        # Press n (select 1)
        # Press enter 
        # Set partition size - 512 MB
        # Type +512M
        # Press Y
        # Change filesystem type Linux Filesystem -> EFI
        # Press t
        # Press 1 (EFI Code)
    
        # Create Root Parition
        # Press n (select 2)
        # Press enter
        # Set Partition size - 30 GB
        # Type +30G
        # Press Y
        
        # Create Home Partition
        # Press n (select 3)
        # Press Enter
        # Set Parition size - Remaining
        # Press Enter
        # Press Y
        
        # Preview Partitions
        # Press p
        # Confirm Write Changes
        # Press w
        
    # Format Partitions
    # Format EFI Parition
    mkfs.fat -F32 /dev/sda# # e.g. /dev/sda1
        
    # Format Root Partition
    mkfs.ext4 /dev/sda# # e.g. /dev/sda2
        
    # Format Home Partition
    mkfs.ext4 /dev/sda# # e.g. /dev/sda3
    
    # Mount Root Parition
    mount /dev/sdaX /mnt # e.g. /dev/sda2
    
    # Make Home Directory
    mkdir /mnt/home
    
    # Mount Home Parition
    mount /dev/sdaX /mnt/home # e.g. /dev/sda3
    
    # Make EFI Boot Directory
    mkdir /mnt/boot/EFI
    
    # Mount EFI Parition
    mount /dev/sdaX /mnt/boot/EFI # e.g. /dev/sda1
    
    # Verify Changes
    lsblk
    
    # Install Base System
    pacstrap -i /mnt base
    
    # Follow Prompts
    # Wait
}

function Standard_Paritioning
{
    # Single partition that is bootable (dos mode)
    cfdisk /dev/sd# # /dev/sd# is the disk we want to install to.
    # Select (dos) label type
    # Select (new) 
    # Enter ...
    # Select (bootable)
    # Enter ...
    # Select (write) ... (yes)
    # Enter ...
    # Select (quit)
    # Enter ...

    # Verifiy partition changes
    fdisk -l

    # Format the partition(s) ...
    mkfs.ext4 /dev/sda# # /dev/sda# the devices we want to format

    # Mount the filesystem(s)
    mount /dev/sda# /mnt # /dev/sda# the device we want to install arch to.

    # Verify the mounted filesystem
    lsblk

    # Install the base packages
    pacstrap /mnt base

    # Wait ...
}

# Configure the system
genfstab -U /mnt >> /mnt/etc/fstab

# Verify changes
cat /mnt/etc/fstab

# Chroot
arch-chroot /mnt

# Timezone
ln -sf /usr/share/zoneinfo/Pacific/Auckland /etc/localtime
hwclock --systohc

# Locale
# Uncomment en_US.UTF-8 UTF-8 /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf
# Verify changes
cat /etc/locale.conf

echo RNDM-VM > /etc/hostname

nano /etc/hosts
# Insert
# 127.0.0.1     localhost
# ::1           localhost
# 127.0.1.1     RNDM-XX.localdomain     RNDM-XX

# Initramfs
mkinitcpio -p linux

# Root password
passwd

# Bootloader
function EFI_Grub
{
    # Download grub and 
    pacman -S grub efibootmgr openssh linux-headers intel-ucode (os-prober)
    

    if Remote_Install
    {   
        # Edit sshd_config
        nano /etc/ssh/sshd_config
        
        # Uncomment PermitRootLogin and set to 'yes'
        systemctl enable sshd
    }
    
    # Install grub-efi
    grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB

    # Copy Locale mo
    cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo

    # Make grub
    grub-mkconfig -o /boot/grub/grub.cfg
}


function Standard_Grub
{
    # Download grub
    pacman -S grub

    # Install grub
    grub-install --target=i386-pc /dev/sdX # X Where the root partition is
    
    # Make grub config
    grub-mkconfig -o /boot/grub/grub.cfg
}

# Setup swapfile
fallocate -l 2G /swapfile
# Set Permissions
chmod 600 /swapfile

# Make Swapfile
mkswap /swapfile

# Add to fstab
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

# Exit chroot
exit

if Desktop_Install
{
    # Unmount Partitions
    umount /dev/sda1 # EFI
    umount /dev/sda3 # Home
    umount /dev/sda2 # Root
}
else
{
    # Unmount partitions
    umount /dev/sdxX # X mounted partition e.g. /dev/sda1
}

# Reboot system
reboot

# Login to root

# Enable network
systemctl status dhcpcd
systemctl enable dhcpcd
systemctl start dhcpcd

# Sync Update system
pacman -Syu

# Add new user
useradd -m -U -G wheel -s /bin/bash rndm
passwd rndm

# Install sudo
pacman -S sudo

# Setup sudo
EDITOR=nano visudo
## Uncomment %wheel ALL=(ALL) ALL

# Exit and login to new user



function Virtualbox_Install
{
    # Install guest additions for vm
    sudo pacman -S virtualbox-guest-utils
    # Restart vm
    # Enable vbox service
    sudo systemctl status vboxservice
    sudo systemctl enable vboxservice
    sudo systemctl start vboxservice

    # Load modules into kernel
    modprobe -a vboxguest vboxsf vboxvideo

    # Driver installation
    lspci | grep -e VGA -e 3D
    sudo pacman -Ss xf86-video-vesa
}

function Deskop_Install
{
    
}

sudo pacman -Syu

# Slow boot fix
sudo pacman -S haveged
sudo systemctl enable haveged.service
sudo systemctl start haveged.service

# Install audio
sudo pacman -S pulseaudio pulseaudio-alsa pulseaudio-bluetooth

# Install nvidia
sudo pacman -S nvidia nvidia-utils nvidia-settings

# Install base apps
sudo pacman -S dolphin konsole firefox

# Install xorg server
sudo pacman -S xorg xorg-server xorg-xinit

# Install desktop
sudo pacman -S plasma plasma-desktop

# Install login manager
sudo pacman -S sddm

# Enable Services
sudo systemctl stop dhcpcd
sudo systemctl disable dhcpcd

sudo systemctl enable bluetooth
sudo systemctl enable sddm
sudo systemctl enable NetworkManager

# Install octopi
function Install_Octopi
{
    sudo pacman -S base-devel git

    git clone https://aur.archlinux.org/trizen.git
    cd trizen
    makepkg -si
    cd ..
    rm -R trizen
    
    trizen -S octopi
}


{
    
    pacman -S haveged pulseaudio pulseaudio-alsa pulseaudio-bluetooth nvidia nvidia-utils nvidia-settings lib32-nvidia-utils
}
